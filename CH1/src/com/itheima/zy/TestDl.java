package com.itheima.zy;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestDl {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext applicationContext = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
			//2.通过容器获取userDao实例
			SpeakImpl ss = (SpeakImpl) applicationContext.getBean("speak");
			//3.调用实例中的say()方法
			ss.say();
			
			//2.通过容器获取userDao实例
			SpeakImpl ss2 = (SpeakImpl) applicationContext.getBean("speak2");
			//3.调用实例中的say()方法
			ss2.sayy();
	}

}
