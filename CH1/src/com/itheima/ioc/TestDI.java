package com.itheima.ioc;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
public class TestDI {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1.初始化sporing容器，加载配置文件
		ApplicationContext applicationContext = 
			new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.通过容器获取userDao实例
		UserServiceImpl uu = (UserServiceImpl) applicationContext.getBean("userService");
		//3.调用实例中的say()方法
		uu.say();
	}

}
