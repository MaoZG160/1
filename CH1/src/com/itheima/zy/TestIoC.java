package com.itheima.zy;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestIoC {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//1.初始化sporing容器，加载配置文件
		ApplicationContext applicationContext = 
			new ClassPathXmlApplicationContext("applicationContext.xml");
		//2.通过容器获取实例
		People p = (People) applicationContext.getBean("people");
		//3.调用实例中的say()方法
		p.say();
		
		People p2 = (People) applicationContext.getBean("people2");
		//3.调用实例中的say()方法
		p2.sayy();
	}

}
