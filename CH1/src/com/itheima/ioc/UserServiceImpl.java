package com.itheima.ioc;

public class UserServiceImpl{
	//声明UserDao属性
	private UserDao userDao;
	
	public UserDao getUserDao(){
		return userDao;
	}
	//添加 SuerDao属性的setter方法，用于实现依赖注入
	public void setUserDao(UserDao userDao){
		this.userDao = userDao;
	}
	
	public void say() {
		//调用
		this.userDao.say();
		System.out.println("userService say Hello world!");
	}
	
}
